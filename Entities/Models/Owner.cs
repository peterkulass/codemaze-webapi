﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    [Table("Owner")]
    public class Owner : IEntity
    {
        [Column("OwnerId")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Name is required!")]
        [StringLength(60, ErrorMessage = "Name can´t be longer then 60 characters!")]
        public string? Name { get; set; }
        [Required(ErrorMessage = "Date of birth is required!")]
        public DateTime DateOfBirth { get; set; }
        [Required(ErrorMessage = "Address is required!")]
        [StringLength(100, ErrorMessage = "Address can´t be longer then 100 characters!")]
        public string? Address { get; set; }
        public ICollection<Account>? Accounts { get; set; }

    }
}
